
<?php
include('header.php');
// if(UI_LANGUAGE){

// }

// if(defined("UI_LANGUAGE")){
    // echo "UI_LANGUAGE";
// UI_LANGUAGE = "EN";
if($ui_lan=="EN") {
    define("c_login", "Login");
    define("c_signup", "Sign Up");
    define("c_username", "username :");
    define("c_password", "password :");
    define("c_signupnow", "Sign Up Now");
    define("c_resetPassword", "Reset Password");
    define("c_passwordConfiguration", "Password Configuration :");
    define("c_enterPasswordConfiguration"," Enter Password Configuration");
    define("c_selectRole", "Select Role :");
    define("c_charity", "Charity :");
    define("c_resturant", "Resturant");
    define("c_driver", "Driver");
    define("c_securityQuestion", "Security Question:");
    define("c_answer", "Answer :");

    define("c_alreadyHaveAcount", "Already have an acount ?");
    define("c_enterQuestionAswer","Enter question answer");
    define("c_checkUsername", "Check Username");
    // define("c_securityQuestion", "Security Question:");
    // define("c_answer", "Answer");
    // define("c_checkUsername","جست و جو نام کاربری  ");
    define("c_enterAswer","Enter Answer");
    define("c_checkAnswer","Check answer");
    define("c_enterNewPassword","Enter new password");
    define("c_newPassword","New password :");
    define("c_enterUsername","Enter username");
    define("c_setResetPassword","Set reset password");
    define("c_haveAcount","Have Acount");

}


else
{
    define("c_login", "ورود");
    define("c_signup", "ثبت نام");
    define("c_username", " نام کاربری :");
    define("c_password", " رمزعبور : ");
    define("c_signupnow", "ثبت نام کنید");
    define("c_resetPassword", " فراموشی رمز عبور");
    define("c_passwordConfiguration", "تکرار رمز عبور :");
    define("c_enterPasswordConfiguration"," تکرار رمز عبور را وارد کنید");
    define("c_selectRole", " انتخاب نقش : ");
    define("c_charity", "خیریه");
    define("c_resturant", "رستوران");
    define("c_driver", "راننده");
    define("c_securityQuestion", " سوال امنیتی :");
    define("c_answer", " پاسخ :");
    define("c_alreadyHaveAcount", "حساب کاربری دارید ؟");
    define("c_enterQuestionAswer","پاسخ پرسش امنیتی ");

    
    define("c_checkUsername","جست و جو نام کاربری  ");
    define("c_enterAswer","پاسخ را وارد کنید");
    define("c_checkAnswer","بررسی پاسخ");
    define("c_enterNewPassword","رمز ورود جدید را وارد کنید ");
    define("c_newPassword"," : رمز عبور جدید");
    define("c_enterUsername","نام کاربری را وارد کنید");
    define("c_setResetPassword","ثبت رمز عبور جدید ");
    define("c_haveAcount"," حساب کاربری دارید ");
}
// }

    ?>